package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;

import model.autenticacioUsuari;
import model.blackJack;
import model.caballo;
import model.carreraCavalls;
import model.creaUsuari;
import model.estadistiques;
import model.request;
import model.ruleta;
import model.usuari;
import view.MainView;
import controller.main;

public class server {
	public class Server extends Thread {

		private boolean actiu;
		private Socket socketToServer;
		private ObjectInputStream objectIn;
		private ObjectOutputStream objectOut;
		public config server_config;
		public boolean error = false;
		@SuppressWarnings("unused")
		private main controlador;
		@SuppressWarnings("unused")
		private MainView vista_principal;

		
		public Server(MainView vista_principal, main contr) {

			// Inicialitzem la configuracio del servidor
			server_config = new config();

			try {
				this.actiu = false;
				this.vista_principal = vista_principal;
				this.controlador =  contr;

				// Creem els punts de connexio (sockets d'entrada i sortida) amb la
				// configuracio que hem rebut
				this.socketToServer = new Socket("127.0.0.1", 40000); //Creem el socket
				
				this.objectOut = new ObjectOutputStream(socketToServer.getOutputStream());  
				//Establir una eina per escriure en la part out del socket, 
				//amb lo de dintre del () diem que on envii la info sigui al socket
				
				this.objectIn = new ObjectInputStream(socketToServer.getInputStream()); 
				//Establir una eina per escriure en la part out del socket, 
				//amb lo de dintre del () diem que on envii la info sigui al socket

			} catch (ConnectException e) {
				
				System.out.println("Hi ha hagut una excepcio del tipus ConnectException perque el servidor no s'estava executant");
				error = true;

			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Hi ha hagut una IOException a l'obrir canals");
				error = true;
			}
		}

		//Inicialitzem el servidor
		public void startServerComunication() {
			actiu = true;
			this.start();
		}

		//Interrompem el servidor
		public void stopServerComunication() {
			
			this.actiu = false;
			this.interrupt(); 
			//el que fa es interrumpir el thread i fa que torni a comen�ar el seu run
		}
		
		public void enviaAutenticacio (autenticacioUsuari r){
			try {
				this.objectOut.writeObject(r);
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		
		public void enviaRegistre (creaUsuari r){
			try {
				this.objectOut.writeObject(r);
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		
		public void enviaRequest (request r){
			try {
				this.objectOut.writeObject(r);
			} catch (IOException e){
				e.printStackTrace();
			}
		}

		@SuppressWarnings("unused")
		public void run() {
			//Aquest bucle s'anira repetint mentre la connexio client-servidor duri
			request r = new request();
			
			while (actiu) {
				try {
					
					//Rebem alguna cosa del servidor
					r = (request) objectIn.readObject();
					
					switch(r.getReconeixement()){
						
						case 1:
							break;
							
						case 4:
							autenticacioUsuari r4 = (autenticacioUsuari) r;
							if (r4.isCorrecte()){
								System.out.println("Usuari correcte");
								//controlador.loginCorrecte();
								
								//CAL DONAR FEEDBACK A LA INTERFÍCIE DE QUE TOT OK
								
							}else{
								System.out.println("Usuari o contrasenya incorrectes");
							}
							
							break;
						
						case 6:
							
							estadistiques r6 = (estadistiques) r;
							
							//FEU EL QUE VULGUEU AMB ELLES
							break;
							
						case 3:
							carreraCavalls rr = (carreraCavalls) r;
							ArrayList<caballo> cavalls = rr.getCavall();
							ArrayList<usuari> usuari = rr.getUsu();

							//INFO DEL CAVALL AMB EL SEU RELATIU USUARI
							break;
						
						case 5:
							ruleta ru = (ruleta) r;
							ArrayList<usuari> usuariRuleta = ru.getUser();
							
							//INFO USUARIS QUE PARTICIPEN A LA RULETA
							break;
							
						case 2:
							blackJack bl = (blackJack) r;
							break;
							
						default:
					}		
					System.out.println("Servidor obert");
					
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					stopServerComunication();
					System.out
							.println("Hi ha hagut una excepcio del tipus ClassNotFound");
					error = true;
				} catch (IOException e) {
					stopServerComunication();
					System.err
							.println("Hi ha hagut una excepcio del tipus IOException: "
									+ e.getMessage());
					error = true;
				} catch (NullPointerException e) {
					System.out
							.println("Hi ha hagut una excepcio del tipus NullPointerException perque el servidor no s'estava executant");
					error = true;
					actiu = false;
				}
			}

			// Quan ja no esta actiu, el desactivem
			stopServerComunication();
		}

		public void sendUsuari(usuari usuari_actual) {

			// Quan el controlador detecta accions, crida aquest metode que passa al
			// servidor el que el controlador ha rebut de la vista
			try {
				creaUsuari r= new creaUsuari (usuari_actual);
				this.objectOut.writeObject(r);

			} catch (IOException e) {
				e.printStackTrace();
				stopServerComunication();
				System.out.println("Hi ha hagut una excepcio d'enviament del tipus IOException");
			}
		}

		public void demanaSituacioBlackJack(){
			
			try{
				request r = new request(2);
				this.objectOut.writeObject(r);
				System.out.println("enviem peticio blackJack");
			}catch (IOException e){
				e.printStackTrace();
				stopServerComunication();
				System.out.println("Hi ha hagut una excepcio d'enviament del tipus IOException");
			}
			
		}
		
		public void enviaSituacioBlackJack(usuari u){
			blackJack r = new blackJack();
			r.setU(u);
			System.out.println("Enviem " + r.getU().getPasta() +" diners al server");
			
		}
		
		public void demanaCarreraCavalls(){			
			try{
				request r = new request(3);
				this.objectOut.writeObject(r); 
				System.out.println("enviem peticio carrera");
			}catch (IOException e){
				e.printStackTrace();
				stopServerComunication();
				System.out.println("Hi ha hagut una excepcio d'enviament del tipus IOException");
			}		
		}
		
		public void enviaSituacioCavalls(usuari apostador, caballo c_apostador, int aposta){
			carreraCavalls r = new carreraCavalls();
			apostador.setAposta(aposta);
			r.setApostador(apostador);
			r.setC_apostador(c_apostador);
			System.out.println("Enviem " + r.getApostador().getNom() +" apostant "+apostador.getAposta()+" al cavall "+ r.getC_apostador());
			
		}
		
		public void demanaRuleta(){
			
			try{
				request r = new request(11);
				this.objectOut.writeObject(r);
				System.out.println("enviem peticio de cancons");
			}catch (IOException e){
				e.printStackTrace();
				stopServerComunication();
				System.out.println("Hi ha hagut una excepcio d'enviament del tipus IOException");
			}
			
		}

		public void enviaRuleta(usuari apostador, int ca_apostador, int aposta){
			ruleta r = new ruleta();
			apostador.setAposta(aposta);
			r.setApostador(apostador);
			r.setCa_apostador(ca_apostador);
			System.out.println("Enviem " + r.getApostador().getNom() +" apostant "+apostador.getAposta()+" a "+ r.getCa_apostador());	
		}

	}
}
