package network;

public class config {
	//Inicialment les dades per defecte son aquestes...
	public String ip = "127.0.0.1";
	public int port = 40000;
	
	public config() {
	}
	
	//A peticio de l'usuari, la vista ens pot retornar unes noves dades que fan que s'actualitzin...
	//...les variables anteriors
	public void canviarIp(String ip) {
		this.ip = ip;
	}
	
	public void canviarPort(int port) {
		this.port = port;
	}
}
