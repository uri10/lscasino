package view;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.logica;

import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class Try extends javax.swing.JFrame {
    /**
     * Creates new form frmInicio
     */
	 	private JButton jButton1;
	    private JButton jButton2;
	    private JLabel jLabelsize;
	    public static JPanel jPanel1;
	    public static JLabel lblCaballo1, lblCaballo2, lblCaballo3, lblCaballo4, lblCaballo5, lblCaballo6, lblCaballo7, lblCaballo8, lblCaballo9,
	    lblCaballo10, lblCaballo11, lblCaballo12;
	    public static JLabel lblLinea;
	    public static JTextArea txtR;
	    private JTextField textField;
	    private JTextField textField_1;
	    private JLabel lblWinnerName;
    public Try() {
        initComponents();
    }
  
    private void initComponents() {
        JPanelBackground jPanel1 = new JPanelBackground();
        lblLinea = new javax.swing.JLabel();
        lblCaballo1 = new JLabel();
        lblCaballo2 = new JLabel();
        lblCaballo3 = new JLabel();
        lblCaballo4 = new JLabel();
        lblCaballo5 = new JLabel();
        lblCaballo6 = new JLabel();
        lblCaballo7 = new JLabel();
        lblCaballo8 = new JLabel();
        lblCaballo9 = new JLabel();
        lblCaballo10 = new JLabel();
        lblCaballo11 = new JLabel();
        lblCaballo12 = new JLabel();
        jButton1 = new JButton();
        jButton2 = new JButton();
        jLabelsize = new JLabel();
        txtR = new JTextArea();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 0, 51));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackgroundimg("/fondo.png");

        lblLinea.setIcon(new ImageIcon(getClass().getResource("/linea.png"))); 

        lblCaballo1.setFont(new Font("Tahoma",Font.BOLD, 13)); 
        lblCaballo1.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 

        lblCaballo2.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo2.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 

        lblCaballo3.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo3.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 

        lblCaballo4.setFont(new Font("Tahoma",Font.BOLD, 13)); 
        lblCaballo4.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo5.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo5.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo6.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo6.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo7.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo7.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo8.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo8.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo9.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo9.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo10.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo10.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo11.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo11.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 
        
        lblCaballo12.setFont(new Font("Tahoma", Font.BOLD, 13)); 
        lblCaballo12.setIcon(new ImageIcon(getClass().getResource("/horse.gif"))); 

        jButton1.setText("Start");
        jButton1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        txtR.setEditable(false);
        txtR.setColumns(20);
        txtR.setFont(new java.awt.Font("Monospaced", 0, 11)); 
        txtR.setRows(5);

        jButton2.setText("New");
        jButton2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        
        textField = new JTextField();
        textField.setColumns(10);
        
        JButton btnNewButton = new JButton("Bet");
        
        JLabel lblHorseName = new JLabel("Horse Name");
        lblHorseName.setFont(new Font("Apple Chancery", Font.PLAIN, 16));
        
        textField_1 = new JTextField();
        textField_1.setColumns(10);
        
        JLabel lblMoney = new JLabel("Money");
        lblMoney.setFont(new Font("Apple Chancery", Font.PLAIN, 16));
        
        lblWinnerName = new JLabel("Winner Name");
        lblWinnerName.setFont(new Font("Apple Chancery", Font.PLAIN, 28));
        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1Layout.setHorizontalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(13)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        						.addComponent(lblCaballo1)
        						.addComponent(lblCaballo3)
        						.addComponent(lblCaballo2)
        						.addComponent(lblCaballo4)
        						.addComponent(lblCaballo5)
        						.addComponent(lblCaballo6)
        						.addComponent(lblCaballo7)
        						.addComponent(lblCaballo8)
        						.addComponent(lblCaballo9)
        						.addComponent(lblCaballo10)
        						.addComponent(lblCaballo11)
        						.addComponent(lblCaballo12)))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addContainerGap()
        					.addComponent(jLabelsize, GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)))
        			.addGap(62)
        			.addComponent(lblLinea, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(61)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        						.addComponent(lblMoney)
        						.addComponent(lblHorseName))
        					.addGap(27)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
        						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        						.addComponent(btnNewButton))
        					.addGap(70))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
        						.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
        							.addGap(36)
        							.addComponent(jButton1)
        							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        							.addComponent(jButton2, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))
        						.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
        							.addGap(133)
        							.addComponent(lblWinnerName)))
        					.addContainerGap(74, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
        	jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        		.addGroup(jPanel1Layout.createSequentialGroup()
        			.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addGap(78)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        						.addComponent(lblHorseName)
        						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        						.addComponent(lblMoney)
        						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        					.addGap(18)
        					.addComponent(btnNewButton)
        					.addGap(228)
        					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
        						.addComponent(jButton1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        						.addComponent(jButton2))
        					.addGap(82)
        					.addComponent(lblWinnerName)
        					.addGap(207))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addContainerGap(72, Short.MAX_VALUE)
        					.addComponent(lblCaballo1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.UNRELATED)
        					.addComponent(jLabelsize)
        					.addComponent(lblCaballo2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(lblCaballo3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo4, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo6, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo7, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo8, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo9, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo10, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo11, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
        					.addGap(16)
        					.addComponent(lblCaballo12, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
        				.addGroup(jPanel1Layout.createSequentialGroup()
        					.addContainerGap()
        					.addComponent(lblLinea, GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        			.addGap(45))
        );
        try {
			ImageIO.read(getClass().getResource(""));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        jPanel1.setLayout(jPanel1Layout);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
     
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
    	//Vistar.txtR.setText("->");
    	logica.iniciar();
    	jButton1.setEnabled(false);
        jButton2.setEnabled(true);
    	
    }

    private void jButton2ActionPerformed(ActionEvent evt) {
    	logica.reiniciar();
        lblCaballo1.setLocation(logica.losCaballos[0].getCoorX(),logica.losCaballos[0].getCoorY());
        lblCaballo2.setLocation(logica.losCaballos[1].getCoorX(),logica.losCaballos[1].getCoorY());
        lblCaballo3.setLocation(logica.losCaballos[2].getCoorX(),logica.losCaballos[2].getCoorY());
        lblCaballo4.setLocation(logica.losCaballos[3].getCoorX(),logica.losCaballos[3].getCoorY());
        lblCaballo5.setLocation(logica.losCaballos[4].getCoorX(),logica.losCaballos[4].getCoorY());
        lblCaballo6.setLocation(logica.losCaballos[5].getCoorX(),logica.losCaballos[5].getCoorY());
        lblCaballo7.setLocation(logica.losCaballos[6].getCoorX(),logica.losCaballos[6].getCoorY());
        lblCaballo8.setLocation(logica.losCaballos[7].getCoorX(),logica.losCaballos[7].getCoorY());
        lblCaballo9.setLocation(logica.losCaballos[8].getCoorX(),logica.losCaballos[8].getCoorY());
        lblCaballo10.setLocation(logica.losCaballos[9].getCoorX(),logica.losCaballos[9].getCoorY());
        lblCaballo11.setLocation(logica.losCaballos[10].getCoorX(),logica.losCaballos[10].getCoorY());
        lblCaballo12.setLocation(logica.losCaballos[11].getCoorX(),logica.losCaballos[11].getCoorY());
        jButton1.setEnabled(true);
        jButton2.setEnabled(false);
    }
    
    private void formWindowOpened(WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    	jButton2.setEnabled(false);
        logica.registrarCaballos();
        lblCaballo1.setText(logica.losCaballos[0].getNombre());
        lblCaballo2.setText(logica.losCaballos[1].getNombre());
        lblCaballo3.setText(logica.losCaballos[2].getNombre());
        lblCaballo4.setText(logica.losCaballos[3].getNombre());
        lblCaballo5.setText(logica.losCaballos[4].getNombre());
        lblCaballo6.setText(logica.losCaballos[5].getNombre());
        lblCaballo7.setText(logica.losCaballos[6].getNombre());
        lblCaballo8.setText(logica.losCaballos[7].getNombre());
        lblCaballo9.setText(logica.losCaballos[8].getNombre());
        lblCaballo10.setText(logica.losCaballos[9].getNombre());
        lblCaballo11.setText(logica.losCaballos[10].getNombre());
        lblCaballo12.setText(logica.losCaballos[11].getNombre());
    }
}

