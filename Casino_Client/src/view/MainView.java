package view;

import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.JButton;

import controller.controladorBotons;

import java.awt.Font;

@SuppressWarnings("serial")
public class MainView extends JFrame{
	
	private imagMenu jpBackground;
	private JButton btnRegistrarse;
	private JButton btnIniciarSesion;
	private JButton button_1;
	
	public MainView() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 450, 300);
		setSize(1920/2,1080/2);
	
		
		jpBackground = new imagMenu();
		getContentPane().add(jpBackground, BorderLayout.CENTER);
		jpBackground.setLayout(null);
		
		btnIniciarSesion = new JButton("Iniciar sesion");
		btnIniciarSesion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnIniciarSesion.setBounds(614, 308, 250, 47);
		jpBackground.add(btnIniciarSesion);
		
		button_1 = new JButton("Jugar como invitado");
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		button_1.setBounds(360, 425, 244, 47);
		jpBackground.add(button_1);
		
		btnRegistrarse = new JButton("Registrarse");
		btnRegistrarse.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnRegistrarse.setBounds(100, 308, 250, 47);
		jpBackground.add(btnRegistrarse);
	}
	
	public void assignaControladorBotones(controladorBotons cBotons) {
		btnRegistrarse.addActionListener(cBotons);
		btnIniciarSesion.addActionListener(cBotons);
		btnRegistrarse.setActionCommand("Registro");
		btnIniciarSesion.setActionCommand("login");
	}
}
