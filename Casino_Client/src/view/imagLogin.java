package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class imagLogin extends JPanel{
private BufferedImage image;
	
	public imagLogin() {
        URL resource = getClass().getResource("/login.jpg");
        try {
        	image = ImageIO.read(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), this);
        
    }
}
