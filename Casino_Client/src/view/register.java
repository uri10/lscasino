package view;
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPasswordField;

import model.usuari;
import controller.controladorBotons;

import java.awt.Color;

@SuppressWarnings("serial")
public class register extends JFrame {

	private JTextField jtfUsername;
	private JButton jb1;
	private JTextField jtfNombre;
	private JLabel lblNombre;
	private JLabel lblApellido;
	private JLabel lblApellido_1;
	private JPasswordField pfContrasena1;
	private JTextField jtfApellido1;
	private JTextField jtfApellido2;
	private JPasswordField pfContrasena;
	
	private imagRegister jpBackground;
	private usuari usuari;
	
	public register() {
		
		setTitle("Registar usuario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 438, 435);
		
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
		
		jpBackground = new imagRegister();
		getContentPane().add(jpBackground, BorderLayout.CENTER);
		jpBackground.setLayout(null);
		
		jtfUsername = new JTextField();
		jtfUsername.setFont(new Font("Tahoma", Font.PLAIN, 12));
		jtfUsername.setBounds(180, 37, 203, 27);
		jpBackground.add(jtfUsername);
		jtfUsername.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(82, 42, 77, 14);
		jpBackground.add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Confirma Contrase\u00F1a:");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(17, 237, 142, 14);
		jpBackground.add(lblPassword);
		
		jb1 = new JButton("Crear usuario");
		jb1.setFont(new Font("Tahoma", Font.BOLD, 15));
		jb1.setBounds(129, 309, 170, 46);
		jpBackground.add(jb1);
		
		JLabel lblEnterPassword = new JLabel("Contrase\u00F1a:");
		lblEnterPassword.setForeground(Color.WHITE);
		lblEnterPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEnterPassword.setBounds(74, 198, 85, 14);
		jpBackground.add(lblEnterPassword);
		
		jtfNombre = new JTextField();
		jtfNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		jtfNombre.setColumns(10);
		jtfNombre.setBounds(180, 75, 203, 27);
		jpBackground.add(jtfNombre);
		
		lblNombre = new JLabel("Nombre:");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNombre.setBounds(89, 80, 70, 14);
		jpBackground.add(lblNombre);
		
		lblApellido = new JLabel("Apellido 1:");
		lblApellido.setForeground(Color.WHITE);
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblApellido.setBounds(82, 122, 70, 14);
		jpBackground.add(lblApellido);
		
		lblApellido_1 = new JLabel("Apellido 2:");
		lblApellido_1.setForeground(Color.WHITE);
		lblApellido_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblApellido_1.setBounds(82, 160, 70, 14);
		jpBackground.add(lblApellido_1);
		
		pfContrasena1 = new JPasswordField();
		pfContrasena1.setBounds(180, 233, 203, 27);
		jpBackground.add(pfContrasena1);
		
		jtfApellido1 = new JTextField();
		jtfApellido1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		jtfApellido1.setColumns(10);
		jtfApellido1.setBounds(180, 117, 203, 27);
		jpBackground.add(jtfApellido1);
		
		jtfApellido2 = new JTextField();
		jtfApellido2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		jtfApellido2.setColumns(10);
		jtfApellido2.setBounds(180, 155, 203, 27);
		jpBackground.add(jtfApellido2);
		
		pfContrasena = new JPasswordField();
		pfContrasena.setBounds(180, 194, 203, 27);
		jpBackground.add(pfContrasena);
		
		
	}
	
	public void assignaControladorBotones(controladorBotons cBotons) {
		jb1.addActionListener(cBotons);
		jb1.setActionCommand("Create User");
	}
	
	public void crearUsuario(){
		
		String username = jtfUsername.getText();
		String name = jtfNombre.getText();
		String apellido1 = jtfApellido1.getText();
		String apellido2 = jtfApellido2.getText();
		String password = String.valueOf(pfContrasena.getPassword());
		String password2 = String.valueOf(pfContrasena1.getPassword());
		
		JFrame error = new JFrame();
		error.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		if(username.equals("") || name.equals("") || apellido1.equals("") || (apellido1.equals("") && !apellido2.equals("")) || password.equals("")){
			JOptionPane.showMessageDialog(error, "Todos los campos se deben rellenar", "Message error", JOptionPane.ERROR_MESSAGE);
		}else{
			
			if(!password.equals(password2)){
				JOptionPane.showMessageDialog(error, "La contraseÒas no coinciden", "Message error", JOptionPane.ERROR_MESSAGE);
			}else{
				usuari = new usuari();
				usuari.setNom(jtfUsername.getText());
				usuari.setCognoms(jtfApellido1.getText());
				usuari.setPass(String.valueOf(pfContrasena.getPassword()));
				
				DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				Date dateobj = new Date();
				usuari.setData_registre(df.format(dateobj));
				
				setVisible(false);
			}
		
		}
		
	}
	
	public usuari recuperarUsuario(){
		return usuari;
	}
	
	
}
