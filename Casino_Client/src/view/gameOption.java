package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Color;

import javax.swing.JButton;


@SuppressWarnings("serial")
public class gameOption extends JFrame{
	
	private imagOption jpBackground;
	private JLabel lblNewLabel;
	private JButton btnNewButton;
	private JButton btnCaballos;
	private JButton btnBlackjack;
	private JButton btnGestionarSesion;
	private JButton btnCerrarSesion;
	
	public gameOption() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("LSCasinoCliente - Game Selection");
		setBounds(100, 100, 450, 300);
		setSize(1920/2,1080/2);
		
		jpBackground = new imagOption();
		getContentPane().add(jpBackground, BorderLayout.CENTER);
		jpBackground.setLayout(null);
		
		lblNewLabel = new JLabel("Selecciona un juego!");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 40));
		lblNewLabel.setBounds(264, 53, 437, 68);
		jpBackground.add(lblNewLabel);
		
		btnNewButton = new JButton("Roulette");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(362, 198, 238, 49);
		jpBackground.add(btnNewButton);
		
		btnCaballos = new JButton("Caballos");
		btnCaballos.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCaballos.setBounds(82, 198, 238, 49);
		jpBackground.add(btnCaballos);
		
		btnBlackjack = new JButton("Blackjack");
		btnBlackjack.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnBlackjack.setBounds(637, 198, 238, 49);
		jpBackground.add(btnBlackjack);
		
		JLabel lblO = new JLabel("Configuracion");
		lblO.setForeground(Color.WHITE);
		lblO.setFont(new Font("Tahoma", Font.BOLD, 40));
		lblO.setBounds(338, 303, 301, 68);
		jpBackground.add(lblO);
		
		btnGestionarSesion = new JButton("Gestionar sesion");
		btnGestionarSesion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnGestionarSesion.setBounds(221, 421, 238, 49);
		jpBackground.add(btnGestionarSesion);
		
		btnCerrarSesion = new JButton("Cerrar sesion");
		btnCerrarSesion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCerrarSesion.setBounds(559, 421, 238, 49);
		jpBackground.add(btnCerrarSesion);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		
	}
}
