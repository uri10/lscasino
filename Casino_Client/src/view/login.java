package view;

import controller.controladorBotons;
import model.usuari;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Color;

@SuppressWarnings("serial")
public class login extends JFrame {

	private imagLogin jpBackground;
	private JTextField textField;
	private JPasswordField textField_1;
	private JButton btnNewButton;
	private JLabel lblNewLabel;
	private JLabel lblPassword;
	
	@SuppressWarnings("unused")
	private usuari usuari;

	public login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Iniciar sesion");
		setBounds(100, 100, 650, 406);
		
		jpBackground = new imagLogin();
		getContentPane().add(jpBackground, BorderLayout.CENTER);
		jpBackground.setLayout(null);
		
		btnNewButton = new JButton("Iniciar sesion");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.setBounds(151, 272, 167, 33);
		jpBackground.add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(151, 78, 167, 33);
		jpBackground.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JPasswordField();
		textField_1.setColumns(10);
		textField_1.setBounds(151, 137, 167, 33);
		jpBackground.add(textField_1);
		
		lblNewLabel = new JLabel("Username :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(58, 85, 83, 14);
		jpBackground.add(lblNewLabel);
		
		lblPassword = new JLabel("Password :");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPassword.setBounds(58, 144, 83, 14);
		jpBackground.add(lblPassword);
		
	}
	
	public void checkUser(){
		
	}
	
	public void assignaControladorBotones(controladorBotons cBotons) {
		btnNewButton.addActionListener(cBotons);
		btnNewButton.setActionCommand("inicia sesion");
	}
}
