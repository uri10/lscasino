package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class imagMenu extends JPanel{
	private BufferedImage image;
	private BufferedImage image2;
	
	public imagMenu() {
        URL resource = getClass().getResource("/casino1.jpg");
        URL resource2 = getClass().getResource("/4698.png");
        try {
        	image = ImageIO.read(resource);
            image2 = ImageIO.read(resource2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), this);
        g.drawImage(image2, 350, 50, image2.getWidth(), image2.getHeight(), this);
        
    }
}
