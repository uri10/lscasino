package model;

import java.util.Random;

public class iniciaR {
	
	usuariR u;
	casellaR[] c;
	Random rand;
	private int win;
	
	public iniciaR() {
		
	
		u  = new usuariR();		
		rand = new Random(System.nanoTime());
		
		c = new casellaR[37];

		for (int i = 0; i < 37; i++){
			c[i] = new casellaR(i);
		}	
	}
	
	public void Apostar(int pasta, int cas, int casell, int doc){
		u.apostar(pasta, cas, casell, doc);
	}
	
	public int getBalance (){		
		return u.getDiners();
	}
	
	public int TiraBola(){
	
		win = 0;
		int resultat = rand.nextInt(36);
		win = u.recupera(c[resultat]);
		
		return resultat;
	}
	public int Winner(){return win;}
	
}
