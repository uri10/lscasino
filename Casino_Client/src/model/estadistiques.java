package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class estadistiques extends request implements Serializable{
	private int pasta;
	private String id_user;
	
	public estadistiques(int pasta, String id_user) {
		super(1);
		this.pasta = pasta;
		this.id_user = id_user;
	}
	
	public estadistiques() {
		super(1);
	}

	public int getPasta() {
		return pasta;
	}

	public void setPasta(int pasta) {
		this.pasta = pasta;
	}

	public String getId_user() {
		return id_user;
	}

	public void setId_user(String id_user) {
		this.id_user = id_user;
	}
}
