package model;

public class cardB {
	private suitB suit;
	private valueB value;
	
	public cardB(suitB suit, valueB value) {
		this.suit = suit;
		this.value = value;
	}

	public String toString(){
		return this.suit.toString() + "-" + this.value.toString();
	}
	
	public valueB getValue(){
		return this.value;
	}

}
