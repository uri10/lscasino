package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class creaUsuari extends request implements Serializable{
	private String Id_usuari;
	private String Nom;
	private String Cognoms;
	private String Pass;
	private String Pasta;
	private String data_registre;
	private int aposta;
	@SuppressWarnings("unused")
	private usuari usuari_nou;

	
	public creaUsuari(){
		super(2);
	}
	
	public creaUsuari(usuari usuari_nou){
		super(2);
		this.usuari_nou = usuari_nou;	
	}

	public creaUsuari(String id_usuari, String nom, String cognoms,
			String pass, String pasta, String data_registre, int aposta) {
		super(2);
		Id_usuari = id_usuari;
		Nom = nom;
		Cognoms = cognoms;
		Pass = pass;
		Pasta = pasta;
		this.data_registre = data_registre;
		this.aposta = aposta;
	}

	public String getId_usuari() {
		return Id_usuari;
	}

	public void setId_usuari(String id_usuari) {
		Id_usuari = id_usuari;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getCognoms() {
		return Cognoms;
	}

	public void setCognoms(String cognoms) {
		Cognoms = cognoms;
	}

	public String getPass() {
		return Pass;
	}

	public void setPass(String pass) {
		Pass = pass;
	}

	public String getPasta() {
		return Pasta;
	}

	public void setPasta(String pasta) {
		Pasta = pasta;
	}

	public String getData_registre() {
		return data_registre;
	}

	public void setData_registre(String data_registre) {
		this.data_registre = data_registre;
	}

	public int getAposta() {
		return aposta;
	}

	public void setAposta(int aposta) {
		this.aposta = aposta;
	}
	
	

}
