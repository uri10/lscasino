package model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class usuari implements Serializable{

	private String Pass;
	private String Id_usuari;
	private String Nom;
	private String Cognoms;
	private int Pasta;
	private String data_registre;
	private int aposta;
	private ArrayList<Integer> cavalls = new ArrayList<Integer>();
	private ArrayList<Integer> ruleta = new ArrayList<Integer>();
	private ArrayList<Integer> blackJack = new ArrayList<Integer>();

	public ArrayList<Integer> getCavalls() {
		return cavalls;
	}



	public void setCavalls(ArrayList<Integer> cavalls) {
		this.cavalls = cavalls;
	}



	public ArrayList<Integer> getRuleta() {
		return ruleta;
	}



	public void setRuleta(ArrayList<Integer> ruleta) {
		this.ruleta = ruleta;
	}



	public ArrayList<Integer> getBlackJack() {
		return blackJack;
	}



	public void setBlackJack(ArrayList<Integer> blackJack) {
		this.blackJack = blackJack;
	}



	public usuari(){
		
	}

	

	public usuari(String pass, String id_usuari, String nom, String cognoms,
			int pasta, String data_registre, int aposta) {
		super();
		Pass = pass;
		Id_usuari = id_usuari;
		Nom = nom;
		Cognoms = cognoms;
		Pasta = pasta;
		this.data_registre = data_registre;
		this.aposta = aposta;
	}

	

	public String getPass() {
		return Pass;
	}



	public void setPass(String pass) {
		Pass = pass;
	}



	public String getId_usuari() {
		return Id_usuari;
	}



	public void setId_usuari(String id_usuari) {
		Id_usuari = id_usuari;
	}



	public String getNom() {
		return Nom;
	}



	public void setNom(String nom) {
		Nom = nom;
	}



	public String getCognoms() {
		return Cognoms;
	}



	public void setCognoms(String cognoms) {
		Cognoms = cognoms;
	}



	public int getPasta() {
		return Pasta;
	}



	public void setPasta(int pasta) {
		Pasta = pasta;
	}



	public String getData_registre() {
		return data_registre;
	}



	public void setData_registre(String data_registre) {
		this.data_registre = data_registre;
	}



	public int getAposta() {
		return aposta;
	}



	public void setAposta(int aposta) {
		this.aposta = aposta;
	}


	//funcion que recibira un usuario y contrase�a y verificara que coincida con alguno de la BBDD
	public int verificaUser(String user, String password){
		//variable que nos dira si el user existe o no
		int existeUser=0;
		return 	existeUser;
	}
}


