package model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class carreraCavalls extends request implements Serializable{
	private ArrayList<usuari> usu;
	private ArrayList<caballo> cavall;
	private usuari guanyador;
	private usuari apostador;
	private caballo c_apostador;
	
	public usuari getGuanyador() {
		return guanyador;
	}

	public void setGuanyador(usuari guanyador) {
		this.guanyador = guanyador;
	}

	public usuari getApostador() {
		return apostador;
	}

	public void setApostador(usuari apostador) {
		this.apostador = apostador;
	}

	public caballo getC_apostador() {
		return c_apostador;
	}

	public void setC_apostador(caballo c_apostador) {
		this.c_apostador = c_apostador;
	}

	public carreraCavalls(){
		super(3);
	}

	public carreraCavalls(ArrayList<usuari> usu, ArrayList<caballo> cavall) {
		super(3);
		this.usu = usu;
		this.cavall = cavall;
	}

	public ArrayList<usuari> getUsu() {
		return usu;
	}

	public void setUsu(ArrayList<usuari> usu) {
		this.usu = usu;
	}

	public ArrayList<caballo> getCavall() {
		return cavall;
	}

	public void setCavall(ArrayList<caballo> cavall) {
		this.cavall = cavall;
	}
	
	
}
