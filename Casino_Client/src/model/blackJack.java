package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class blackJack extends request implements Serializable{
	private usuari u;


	
	public blackJack(usuari u) {
		super(2);
		this.u = u;
	}

	public blackJack() {
		super(2);
	}

	public usuari getU() {
		return u;
	}

	public void setU(usuari u) {
		this.u = u;
	}

}
