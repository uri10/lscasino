package model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class ruleta extends request implements Serializable{
	ArrayList<usuari> user;
	int[] casella;
	
	usuari apostador;
	int ca_apostador;
	


	public usuari getApostador() {
		return apostador;
	}

	public void setApostador(usuari apostador) {
		this.apostador = apostador;
	}


	public ruleta(ArrayList<usuari> user, int[] casella) {
		super(5);
		this.user = user;
		this.casella = casella;
	}

	public ArrayList<usuari> getUser() {
		return user;
	}

	public void setUser(ArrayList<usuari> user) {
		this.user = user;
	}


	public int[] getCasella() {
		return casella;
	}

	public void setCasella(int[] casella) {
		this.casella = casella;
	}

	public int getCa_apostador() {
		return ca_apostador;
	}

	public void setCa_apostador(int ca_apostador) {
		this.ca_apostador = ca_apostador;
	}

	public ruleta() {
		super(5);
		// TODO Auto-generated constructor stub
	}

}
