package controller;
import model.usuari;
import view.MainView;
import view.gameOption;
import view.login;
import view.register;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class controladorBotons implements ActionListener{
	
	private MainView vista;
	private register vista2;
	private login vista3;
	private gameOption vista4;
	
	@SuppressWarnings("unused")
	private usuari usuari;
	public controladorBotons(MainView vista, register vista2, login vista3, gameOption vista4){
		this.vista = vista;
		this.vista2 = vista2;
		this.vista3 = vista3;
		this.vista4 = vista4;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		String button = e.getActionCommand();
		
		if(button.equals("Registro")){
			usuari = new usuari();
			vista2.setSize(650,430);
			vista2.setResizable(false);
			vista2.setVisible(true);
			
		}
		
		if(button.equals("Create User")){
			vista2.crearUsuario();
		}
		
		if(button.equals("login")){
			vista3.setSize(650, 406);
			vista3.setResizable(false);
			vista3.setVisible(true);
		}
		
		if(button.equals("inicia sesion")){
			vista3.setVisible(false);
			vista.setVisible(false);
			vista4.setVisible(true);
		}
		
		
	}
}
