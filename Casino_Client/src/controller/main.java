package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

import view.MainRuleta;
import view.MainView;
import view.gameOption;
import view.login;
import view.register;
import model.blackJackB;
import model.iniciaR;
import network.server.Server;

public class main implements ActionListener {

	private Server servidor;
	private MainView interficie;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			
			public void run() {
	
				register r = new register();
				MainView m = new MainView();
				login l = new login();
				gameOption go = new gameOption();

				m.setTitle("LSCasinoCliente - Menu");
				m.setVisible(true);
				
				controladorBotons controlador = new controladorBotons(m,r,l,go);
				m.assignaControladorBotones(controlador);
				r.assignaControladorBotones(controlador);
				l.assignaControladorBotones(controlador);
				/*blackJackB b = new blackJackB();
				MainRuleta vista = new MainRuleta();
				
				iniciaR model = new iniciaR();
				
				listenerBotonsR controlador = new listenerBotonsR(vista, model);
								
				vista.registreControladorBoto(controlador);
								
				vista.setVisible(true);*/
					
				
			}
		});
	}
	
	public void setServer(Server servidor){
		this.servidor = servidor;
		servidor.startServerComunication();
	}
	
	public void actionPerformed(ActionEvent event) {
		
	}

}