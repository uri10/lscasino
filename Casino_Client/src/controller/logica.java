package controller;
import java.util.Random;

import model.caballo;
import view.Vista;

public class logica {
    
    public static caballo[] losCaballos;
    static Random rnd;
    static int[] vel;
    
    static public void reiniciar(){
    int[] vel={240,250,255,245};
    int[] ava={13,15,16,14};
     //Vel
    	
        losCaballos[0].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[1].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[2].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[3].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[4].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[5].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[6].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[7].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[8].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[9].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[10].setVelocidad(vel[rnd.nextInt(4)]);
        losCaballos[11].setVelocidad(vel[rnd.nextInt(4)]);
        
        //avances
        losCaballos[0].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[1].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[2].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[3].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[4].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[5].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[6].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[7].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[8].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[9].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[10].setAvance(ava[rnd.nextInt(4)]);
        losCaballos[11].setAvance(ava[rnd.nextInt(4)]);
    	
    }//
    
    static public void registrarCaballos(){
        rnd= new Random();
        int[] vel={210,220,240,250,255,245};
        int[] ava={11,12,13,15,16,14};
        losCaballos = new caballo[12];
        
        losCaballos[0] = new caballo(0,0,0,0,0,"-");
        losCaballos[1] = new caballo(0,0,0,0,0,"-");
        losCaballos[2] = new caballo(0,0,0,0,0,"-");
        losCaballos[3] = new caballo(0,0,0,0,0,"-");
        losCaballos[4] = new caballo(0,0,0,0,0,"-");
        losCaballos[5] = new caballo(0,0,0,0,0,"-");
        losCaballos[6] = new caballo(0,0,0,0,0,"-");
        losCaballos[7] = new caballo(0,0,0,0,0,"-");
        losCaballos[8] = new caballo(0,0,0,0,0,"-");
        losCaballos[9] = new caballo(0,0,0,0,0,"-");
        losCaballos[10] = new caballo(0,0,0,0,0,"-");
        losCaballos[11] = new caballo(0,0,0,0,0,"-");
        //ingreso de datos
        //codigos
        losCaballos[0].setCodigo(101);
        losCaballos[1].setCodigo(102);
        losCaballos[2].setCodigo(103);
        losCaballos[3].setCodigo(104);
        losCaballos[4].setCodigo(105);
        losCaballos[5].setCodigo(106);
        losCaballos[6].setCodigo(107);
        losCaballos[7].setCodigo(108);
        losCaballos[8].setCodigo(109);
        losCaballos[9].setCodigo(110);
        losCaballos[10].setCodigo(111);
        losCaballos[11].setCodigo(112);
        //nombres
        losCaballos[0].setNombre("Horse 1");
        losCaballos[1].setNombre("Horse 2");
        losCaballos[2].setNombre("Horse 3");
        losCaballos[3].setNombre("Horse 4");
        losCaballos[4].setNombre("Horse 5");
        losCaballos[5].setNombre("Horse 6");
        losCaballos[6].setNombre("Horse 7");
        losCaballos[7].setNombre("Horse 8");
        losCaballos[8].setNombre("Horse 9");
        losCaballos[9].setNombre("Horse 10");
        losCaballos[10].setNombre("Horse 11");
        losCaballos[11].setNombre("Horse 12");
        //CoordX
        losCaballos[0].setCoorX(Vista.lblCaballo1.getLocation().x);
        losCaballos[1].setCoorX(Vista.lblCaballo2.getLocation().x);
        losCaballos[2].setCoorX(Vista.lblCaballo3.getLocation().x);
        losCaballos[3].setCoorX(Vista.lblCaballo4.getLocation().x);
        losCaballos[4].setCoorX(Vista.lblCaballo5.getLocation().x);
        losCaballos[5].setCoorX(Vista.lblCaballo6.getLocation().x);
        losCaballos[6].setCoorX(Vista.lblCaballo7.getLocation().x);
        losCaballos[7].setCoorX(Vista.lblCaballo8.getLocation().x);
        losCaballos[8].setCoorX(Vista.lblCaballo9.getLocation().x);
        losCaballos[9].setCoorX(Vista.lblCaballo10.getLocation().x);
        losCaballos[10].setCoorX(Vista.lblCaballo11.getLocation().x);
        losCaballos[11].setCoorX(Vista.lblCaballo12.getLocation().x);
        //CoordX
        losCaballos[0].setCoorY(Vista.lblCaballo1.getLocation().y);
        losCaballos[1].setCoorY(Vista.lblCaballo2.getLocation().y);
        losCaballos[2].setCoorY(Vista.lblCaballo3.getLocation().y);
        losCaballos[3].setCoorY(Vista.lblCaballo4.getLocation().y);
        losCaballos[4].setCoorY(Vista.lblCaballo5.getLocation().y);
        losCaballos[5].setCoorY(Vista.lblCaballo6.getLocation().y);
        losCaballos[6].setCoorY(Vista.lblCaballo7.getLocation().y);
        losCaballos[7].setCoorY(Vista.lblCaballo8.getLocation().y);
        losCaballos[8].setCoorY(Vista.lblCaballo9.getLocation().y);
        losCaballos[9].setCoorY(Vista.lblCaballo10.getLocation().y);
        losCaballos[10].setCoorY(Vista.lblCaballo11.getLocation().y);
        losCaballos[11].setCoorY(Vista.lblCaballo12.getLocation().y);
        //Vel
        losCaballos[0].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[1].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[2].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[3].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[4].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[5].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[6].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[7].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[8].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[9].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[10].setVelocidad(vel[rnd.nextInt(6)]);
        losCaballos[11].setVelocidad(vel[rnd.nextInt(6)]);
        
        losCaballos[0].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[1].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[2].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[3].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[4].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[5].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[6].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[7].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[8].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[9].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[10].setAvance(ava[rnd.nextInt(6)]);
        losCaballos[11].setAvance(ava[rnd.nextInt(6)]);
        //
        losCaballos[0].setTiempo(0);
        losCaballos[1].setTiempo(0);
        losCaballos[2].setTiempo(0);
        losCaballos[3].setTiempo(0);
        losCaballos[4].setTiempo(0);
        losCaballos[5].setTiempo(0);
        losCaballos[6].setTiempo(0);
        losCaballos[7].setTiempo(0);
        losCaballos[8].setTiempo(0);
        losCaballos[9].setTiempo(0);
        losCaballos[10].setTiempo(0);
        losCaballos[11].setTiempo(0);
        
    }//
     
    public static void iniciar(){
    	
        horseThread h1= new horseThread(losCaballos[0].getNombre());
        h1.start();
        
        horseThread h2= new horseThread(losCaballos[1].getNombre());
        h2.start();
        
        horseThread h3= new horseThread(losCaballos[2].getNombre());
        h3.start();
        
        horseThread h4= new horseThread(losCaballos[3].getNombre());
        h4.start();
        
        horseThread h5= new horseThread(losCaballos[4].getNombre());
        h5.start();
        
        horseThread h6= new horseThread(losCaballos[5].getNombre());
        h6.start();
       
        horseThread h7= new horseThread(losCaballos[6].getNombre());
        h7.start();
        
        horseThread h8= new horseThread(losCaballos[7].getNombre());
        h8.start();
        
        horseThread h9= new horseThread(losCaballos[8].getNombre());
        h9.start();
        
        horseThread h10= new horseThread(losCaballos[9].getNombre());
        h10.start();
        
        horseThread h11= new horseThread(losCaballos[10].getNombre());
        h11.start();
        
        horseThread h12= new horseThread(losCaballos[11].getNombre());
        h12.start();
        
    }//iniciar           
      
    
    }//class
