package vista;
import javax.swing.JFrame;

import controller.mainController;

@SuppressWarnings("serial")
public class MainViewServer extends JFrame {

	// Relacions amb els panells que es mostren al menu
	private UserViewServer vista_usuari;

	// Layout per tal de mostrar el conjunt de panells


	//Aquest metode crea i gestiona el cardlayout
	public MainViewServer () {
		
	}

	@SuppressWarnings("unused")
	private void creaMenu() {

		//Creem un menu amb un desplegable que conte dues opcions
		
	}

	public void changePanel(String which) {
	}

	public void registerController(mainController c) {
		
		//Posem etiquetes per poder identificar les diferents opcions del menu
		
		//Registrem el Controller a les diferents opcions del menu
		
		//Passem el Controller als panells secundaris per tal de registrar la resta de components
		
		
	}
	
	public UserViewServer getPanellUsuaris() {
		return vista_usuari;
	}
}
