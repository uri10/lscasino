package vista;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import controller.mainController;
import model.usuari;

@SuppressWarnings("serial")
public class UserViewServer extends JPanel {
	private JButton esborra;
	
	public UserViewServer() {
				
	}

	//Aquest metode es crida des del servidor, quan hem rebut un nou element del usuari
	public void refrescaLlista(ArrayList<usuari> usuaris) {
		
	}

	//Aquest metode es crida des del servidor, quan hem rebut un nou element dels detalls
	public void refrescaDetalls(String detalls) {
			//Aquest codi s'ha traslladat al metode anterior
	}
	
	public void registerController(mainController controlador) {
		
        //Creem les comandes i les relacionem amb un listener
        esborra.setActionCommand("ESBORRA_USUARI");
        esborra.addActionListener(controlador);     

	}
}