package network;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.usuari;


public class BDD {
	
	public void afegeixUsuari (usuari u){
				//Inici conexió amb parametres
				ConectorDB conn = new ConectorDB("root", "1234", "dpo", 3306);
				//Connexio a bdd
				conn.connect();
				//Insercio a bdd
				conn.insertQuery("INSERT INTO User (idUser,name,pass,money,registration) VALUES ('"+u.getId_usuari()+"','"+u.getPass()+"','"+u.getAposta()+"','"+u.getData_registre()+"' )"); 
				//Ens desconectem de la BBDD una vegada no la necessitem
				conn.disconnect();
	}
	
	public void eliminaUsuari (usuari u){
		//Inici conexió amb parametres
		ConectorDB conn = new ConectorDB("root", "1234", "dpo", 3306);
		//Connexio a bdd
		conn.connect();
		//Eliminem un registre de la BBDD
		conn.deleteQuery("DELETE FROM User WHERE idUser='"+u.getId_usuari()+"')");
		//Ens desconectem de la BBDD una vegada no la necessitem
		conn.disconnect();
	}
	
	
	@SuppressWarnings("finally")
	public String  getIdAssociada(int i){
		ResultSet consulta;
		String identificador = null;
		//Inici conexió amb parametres
		ConectorDB conn = new ConectorDB("root", "1234", "dpo", 3306);
		//Connexio a bdd
		conn.connect();
		consulta = conn.selectQuery("SELECT Id_Usuari, FROM Usuari WHERE Id_Usuari='"+i+"'");
		try {
			identificador = (String) consulta.getObject("Id_Usuari");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			//Ens desconectem de la BBDD una vegada no la necessitem
			conn.disconnect();
			return identificador;
		}
		
		
		
	}
	
	@SuppressWarnings("finally")
	public String getPassword(String id){
		ResultSet consulta;
		String pass=null;
		//Inici conexió amb parametres
		ConectorDB conn = new ConectorDB("root", "1234", "dpo", 3306);
		//Connexio a bdd
		conn.connect();
		consulta = conn.selectQuery("SELECT Id_usuari, Pass FROM Usuari WHERE Id_usuari= '"+id+"' GROUP BY Id_Usuari");
		
		try {
			//Recorrem el ResultSet que ens retorna el selectQuery i agafem el que coincideixi
			while (consulta.next())
			{
			    //Agafem el password
				System.out.println("hola entro");
				pass = (String) consulta.getObject("Pass");
			}
		} catch (SQLException e) {
			System.out.println("Problema al recuperar les dades...");
		}
		finally{
			//Ens desconectem de la BBDD una vegada no la necessitem
			conn.disconnect();
			return pass;
		}
	}
	
	@SuppressWarnings("finally")
	public ArrayList<usuari> consultaTotsUsuaris (){
		
		ResultSet consulta;
		ArrayList<usuari> usu = new ArrayList<usuari>();
		ConectorDB conn = new ConectorDB("root", "1234", "dpo", 3306);
		conn.connect();
		consulta = conn.selectQuery("SELECT * FROM Usuari");
		
		try {
			//Recorrem el ResultSet que ens retorna el selectQuery i agafem el que coincideixi
			while (consulta.next())
			{
			    //Agafem el password
				usuari auxusu = new usuari();
				auxusu.setId_usuari((String) consulta.getObject("idUser"));
				auxusu.setPass((String) consulta.getObject("pass"));
				auxusu.setData_registre((String) consulta.getObject("registration"));
				auxusu.setNom((String) consulta.getObject("name"));
				usu.add(auxusu);
			}
			
		} catch (SQLException e) {
			System.out.println("Problema al recuperar les dades...");
			usu = null;
		}
		finally{
			//Ens desconectem de la BBDD una vegada no la necessitem
			conn.disconnect();	
			System.out.println("Ha entrat a la base");
			return usu;
		}
		
	}

}

