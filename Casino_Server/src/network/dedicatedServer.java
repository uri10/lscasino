package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;

import model.autenticacioUsuari;
import model.estadistiques;
import model.request;
import model.usuari;
import model.creaUsuari;
import jdk.nashorn.internal.ir.RuntimeNode.Request;

import com.sun.corba.se.spi.activation.Server;

public class dedicatedServer extends Thread{
    private boolean isOn;
    private Socket sClient;
    private LinkedList<dedicatedServer> clients;
    @SuppressWarnings("unused")
	private request r;
    private Server server;

    public dedicatedServer(request r, LinkedList<dedicatedServer> clients, Socket sClient, Server server){
            this.r = r;
            this.sClient = sClient;
            this.server =  server;
            this.clients = clients;
            this.clients.add(this);
    }

    public void startDedicatedServer() {
            System.out.println("Arranca la festa");
            // iniciem el servidor dedicat
            isOn = true;
            this.start();
    }

    public void stopDedicatedServer() {
            // aturem el servidor dedicat
            this.isOn = false;
            this.interrupt();
    }

    public void run() {
            try {
                    // creem els canals de comunicacio

            		ObjectInputStream lectura = new ObjectInputStream(sClient.getInputStream());
					ObjectOutputStream escritura = new ObjectOutputStream(sClient.getOutputStream());
					BDD bd = new BDD();
                    updateAllClients();


                    while (isOn) {
                    	System.out.println("Entro again");
                    	try {
        					System.out.println("Dins del try");
        					request r = (request) lectura.readObject();
        					
        					switch(r.getReconeixement()){
        					case 1:
        						/*r = (request) lectura.readObject();
        						if (bd.getIdAssociada(r.getId())==null){
        							bd.afegeixLlista(r.getAux(), r.getId_usuari());
        	
        						}else{
        							
        							System.out.println("Usuari incorrecte");
        						}*/
        						break;

        					case 4:
        						String auxpass = bd.getPassword(((autenticacioUsuari) r).getId_user());
        						System.out.println(((autenticacioUsuari) r).getPass() + ((autenticacioUsuari) r).getId_user());
        						System.out.println("password1: "+ auxpass);
        						System.out.println("password2 : "+((autenticacioUsuari)r).getPass());
        						if (auxpass!=null){
        							if (auxpass.equals( ((autenticacioUsuari)r).getPass()) ){ 
        								((autenticacioUsuari)r).setCorrecte(true);
        								escritura.writeObject(r);
        							}else{
        								System.out.println("ERROOOOOR");
        								((autenticacioUsuari)r).setCorrecte(false);
        								escritura.writeObject(r);
        							}
        						}else{
        							System.out.println("Usuari incorrecte");
        							System.out.println("ERROOOOOR");
        							((autenticacioUsuari)r).setCorrecte(false);
        							escritura.writeObject(r);
        						}
        						break;
        					case 5:
        						usuari u = new usuari(((creaUsuari) r).getId_usuari(),((creaUsuari) r).getNom(),((creaUsuari) r).getCognoms(),((creaUsuari) r).getPass(),((creaUsuari) r).getPasta(),((creaUsuari) r).getData_registre(),((creaUsuari) r).getAposta());
        						bd.afegeixUsuari(u);
        						break;
        					case 6:
        						r = (estadistiques) lectura.readObject();
        						//estad = bd.descarregaLlistesUsuari(((estadistiques) r).consultaTotsUsuaris());
        						escritura.writeObject(r);
        						break;
        					case 3:
        						
        						break;
        					case 2:
        						
        						break;
        					default:
        					}
        					
        				} catch (ClassNotFoundException e) {
        					System.out.println("Error1");
        					// TODO Auto-generated catch block
        					e.printStackTrace();
        				}	
            				
                    }
            } catch (IOException e1) {
                    // en cas derror aturem el servidor dedicat
                    stopDedicatedServer();
                    // eliminem el servidor dedicat del conjunt de servidors dedicats
                    //OJOOOOOOOO ACTIVAR/////////////server.getDServers().remove(this);
                    // invoquem el metode del servidor que mostra els servidors dedicats actuals
                    //OJOOOOOOOO ACTIVAR/////////////server.showClients();
            }
    }

    /**Procediment sincronitzat que envia a tots els clients al informacio del ranking de manera que encara que sigui cridada per dos clients alhora,
     * fara primer un i despres l'altre per tal de verificar la validesa del ranking enviat en tot moment.
     *
     */
    public synchronized void updateAllClients(){
            for (int i =0 ; i< clients.size() ; i++) {
                    // recuperem el canal de sortida del servidor dedicat
                    // per tal de contactar amb el client

                    //CAL ACTUALITZAR DARRERA CONNEXIÓ USUARIS

            }
    }

}
