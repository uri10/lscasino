package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

import com.sun.corba.se.spi.activation.Server;

import model.request;

public class server extends Thread {

	private boolean active;
	private ServerSocket sSocket;
	private LinkedList<dedicatedServer> dServers;
	private request r;
	

	public server() {
		super();
		try {
			
			this.active = false;
			this.sSocket = new ServerSocket(networkConfiguration.SERVER_PORT);
			this.dServers = new LinkedList<dedicatedServer>();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Posem en marxa el servidor
	public void startServer() {
		active = true;
		System.out.println("Servidor iniciat...");
		this.start();
	}

	// Aturem el servidor
	public void stopServer() {
		active = false;
		this.interrupt();
	}

	// Mostem en consola quants clients tenim
	// Aquest metode es crida cada cop que hi ha una variacio en el numero de
	// clients connectats

	public void showClients() {
        System.out.println("***** SERVER ***** (" + dServers.size() +" clients / dedicated servers running)");
	}
	
	public void run() {
		 System.out.println("Arranquem");
         active = true;
         while (active) {
                 try {
                         Socket sClient = sSocket.accept();
                         System.out.println("Nou client");
                         dedicatedServer newClient = new dedicatedServer(r, dServers, sClient, (Server) this); //LA REQUEST QUE LI PASSEM HA DE SER CERTA
                         dServers.add(newClient);
                         // engegem el servidor dedicat
                         newClient.startDedicatedServer();
                         showClients();
                 } catch (IOException e) {
                         e.printStackTrace();
                 }
         }
         // aturem tots els servidors dedicats creats
         // quan ja no atenem mes peticions de clients
         for (dedicatedServer dServer : dServers) {
                 dServer.stopDedicatedServer();
         }
	}
	
	public LinkedList<dedicatedServer> getDServers() {
		return dServers;
	}

	public void setDServers(LinkedList<dedicatedServer> dServers) {
		this.dServers = dServers;
	}

}
