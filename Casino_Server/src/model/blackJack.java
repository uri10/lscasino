package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class blackJack extends request implements Serializable{
	private int diners;

	public blackJack(int diners) {
		super(2);
		this.diners = diners;
	}
	
	public blackJack() {
		super(2);
	}

	public int getDiners() {
		return diners;
	}

	public void setDiners(int diners) {
		this.diners = diners;
	}
}
